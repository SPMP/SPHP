---- Minecraft Crash Report ----
// Oh - I know what I did wrong!

Time: 1/28/17 7:03 PM
Description: There was a severe problem during mod loading that has caused the game to fail

cpw.mods.fml.common.LoaderException: java.lang.NoSuchMethodError: appeng.api.features.IInscriberRegistry.getRecipes()Ljava/util/List;
	at cpw.mods.fml.common.LoadController.transition(LoadController.java:163)
	at cpw.mods.fml.common.Loader.initializeMods(Loader.java:744)
	at cpw.mods.fml.client.FMLClientHandler.finishMinecraftLoading(FMLClientHandler.java:311)
	at net.minecraft.client.Minecraft.func_71384_a(Minecraft.java:552)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:878)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:236)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:297)
	at org.multimc.EntryPoint.listen(EntryPoint.java:162)
	at org.multimc.EntryPoint.main(EntryPoint.java:53)
Caused by: java.lang.NoSuchMethodError: appeng.api.features.IInscriberRegistry.getRecipes()Ljava/util/List;
	at thaumicenergistics.common.registries.AEAspectRegister.getInscriberRecipes(AEAspectRegister.java:902)
	at thaumicenergistics.common.registries.AEAspectRegister.registerAEAspects(AEAspectRegister.java:1336)
	at thaumicenergistics.common.ThaumicEnergistics.postInit(ThaumicEnergistics.java:163)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at cpw.mods.fml.common.FMLModContainer.handleModStateEvent(FMLModContainer.java:532)
	at sun.reflect.GeneratedMethodAccessor4.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at com.google.common.eventbus.EventSubscriber.handleEvent(EventSubscriber.java:74)
	at com.google.common.eventbus.SynchronizedEventSubscriber.handleEvent(SynchronizedEventSubscriber.java:47)
	at com.google.common.eventbus.EventBus.dispatch(EventBus.java:322)
	at com.google.common.eventbus.EventBus.dispatchQueuedEvents(EventBus.java:304)
	at com.google.common.eventbus.EventBus.post(EventBus.java:275)
	at cpw.mods.fml.common.LoadController.sendEventToModContainer(LoadController.java:212)
	at cpw.mods.fml.common.LoadController.propogateStateMessage(LoadController.java:190)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at com.google.common.eventbus.EventSubscriber.handleEvent(EventSubscriber.java:74)
	at com.google.common.eventbus.SynchronizedEventSubscriber.handleEvent(SynchronizedEventSubscriber.java:47)
	at com.google.common.eventbus.EventBus.dispatch(EventBus.java:322)
	at com.google.common.eventbus.EventBus.dispatchQueuedEvents(EventBus.java:304)
	at com.google.common.eventbus.EventBus.post(EventBus.java:275)
	at cpw.mods.fml.common.LoadController.distributeStateMessage(LoadController.java:119)
	at cpw.mods.fml.common.Loader.initializeMods(Loader.java:742)
	... 18 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Linux (amd64) version 4.8.13-1-ARCH
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 1188548176 bytes (1133 MB) / 3061841920 bytes (2920 MB) up to 3817865216 bytes (3641 MB)
	JVM Flags: 2 total; -Xms1024m -Xmx4096m
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1614 Optifine OptiFine_1.7.10_HD_U_D6 109 mods loaded, 109 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJ	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJ	FML{7.10.99.99} [Forge Mod Loader] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJ	Forge{10.13.4.1614} [Minecraft Forge] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJ	appliedenergistics2-core{rv3-beta-6} [Applied Energistics 2 Core] (minecraft.jar) 
	UCHIJ	Aroma1997Core{1.0.2.16} [Aroma1997Core] (Aroma1997Core-1.7.10-1.0.2.16.jar) 
	UCHIJ	CodeChickenCore{1.0.7.47} [CodeChicken Core] (minecraft.jar) 
	UCHIJ	NotEnoughItems{1.0.5.120} [Not Enough Items] (NotEnoughItems-1.7.10-1.0.5.120-universal.jar) 
	UCHIJ	ThE-core{1.0.0.1} [Thaumic Energistics Core] (minecraft.jar) 
	UCHIJ	ThaumicTinkerer-preloader{0.1} [Thaumic Tinkerer Core] (minecraft.jar) 
	UCHIJ	OpenModsCore{0.9.1} [OpenModsCore] (minecraft.jar) 
	UCHIJ	<CoFH ASM>{000} [CoFH ASM] (minecraft.jar) 
	UCHIJ	FastCraft{1.23} [FastCraft] (fastcraft-1.23.jar) 
	UCHIJ	bspkrsCore{6.16} [bspkrsCore] ([1.7.10]bspkrsCore-universal-6.16.jar) 
	UCHIJ	ArmorStatusHUD{1.28} [ArmorStatusHUD] ([1.7.10]ArmorStatusHUD-client-1.28.jar) 
	UCHIJ	StatusEffectHUD{1.27} [StatusEffectHUD] ([1.7.10]StatusEffectHUD-client-1.27.jar) 
	UCHIJ	IC2{2.2.827-experimental} [IndustrialCraft 2] (industrialcraft-2-2.2.827-experimental.jar) 
	UCHIJ	AdvancedMachines{1.1.6} [IC2 Advanced Machines Addon] (AdvancedMachinesAS-1.7.10.jar) 
	UCHIJ	appliedenergistics2{rv3-beta-6} [Applied Energistics 2] (appliedenergistics2-rv3-beta-6.jar) 
	UCHIJ	bdlib{1.9.2.104} [BD Lib] (bdlib-1.9.2.104-mc1.7.10.jar) 
	UCHIJ	ae2stuff{0.5.0.56} [AE2 Stuff] (ae2stuff-0.5.0.56-mc1.7.10.jar) 
	UCHIJ	Aroma1997CoreHelper{1.0.2.16} [Aroma1997Core|Helper] (Aroma1997Core-1.7.10-1.0.2.16.jar) 
	UCHIJ	Aroma1997sDimension{1.0} [Aroma1997's Dimensional World] (Aroma1997s-Dimensional-World-1.7.10-1.1.0.1.jar) 
	UCHIJ	AromaBackup{0.1.0.0} [AromaBackup] (AromaBackup-1.7.10-0.1.0.0.jar) 
	UCHIJ	AromaBackupRecovery{1.0} [AromaBackup Recovery] (AromaBackup-1.7.10-0.1.0.0.jar) 
	UCHIJ	BiblioCraft{1.11.5} [BiblioCraft] (BiblioCraft[v1.11.5][MC1.7.10].jar) 
	UCHIJ	Mantle{1.7.10-0.3.2.jenkins191} [Mantle] (Mantle-1.7.10-0.3.2b.jar) 
	UCHIJ	Natura{2.2.0} [Natura] (natura-1.7.10-2.2.0.1.jar) 
	UCHIJ	BiomesOPlenty{2.1.0} [Biomes O' Plenty] (BiomesOPlenty-1.7.10-2.1.0.2027-universal.jar) 
	UCHIJ	BiblioWoodsBoP{1.9} [BiblioWoods Biomes O'Plenty Edition] (BiblioWoods[BiomesOPlenty][v1.9].jar) 
	UCHIJ	CoFHCore{1.7.10R3.1.3} [CoFH Core] (CoFHCore-[1.7.10]3.1.3-327.jar) 
	UCHIJ	Forestry{4.2.16.64} [Forestry for Minecraft] (forestry_1.7.10-4.2.16.64.jar) 
	UCHIJ	BiblioWoodsForestry{1.7} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.7].jar) 
	UCHIJ	BiblioWoodsNatura{1.5} [BiblioWoods Natura Edition] (BiblioWoods[Natura][v1.5].jar) 
	UCHIJ	Baubles{1.0.1.10} [Baubles] (Baubles-1.7.10-1.0.1.10.jar) 
	UCHIJ	Thaumcraft{4.2.3.5} [Thaumcraft] (Thaumcraft-1.7.10-4.2.3.5.jar) 
	UCHIJ	Botania{r1.8-249} [Botania] (Botania r1.8-249.jar) 
	UCHIJ	BuildCraft|Core{7.1.19} [BuildCraft] (buildcraft-7.1.19.jar) 
	UCHIJ	BuildCraft|Energy{7.1.19} [BC Energy] (buildcraft-7.1.19.jar) 
	UCHIJ	BuildCraft|Silicon{7.1.19} [BC Silicon] (buildcraft-7.1.19.jar) 
	UCHIJ	BuildCraft|Builders{7.1.19} [BC Builders] (buildcraft-7.1.19.jar) 
	UCHIJ	BuildCraft|Robotics{7.1.19} [BC Robotics] (buildcraft-7.1.19.jar) 
	UCHIJ	BuildCraft|Transport{7.1.19} [BC Transport] (buildcraft-7.1.19.jar) 
	UCHIJ	BuildCraft|Factory{7.1.19} [BC Factory] (buildcraft-7.1.19.jar) 
	UCHIJ	ThermalFoundation{1.7.10R1.2.5} [Thermal Foundation] (ThermalFoundation-[1.7.10]1.2.5-115.jar) 
	UCHIJ	ThermalExpansion{1.7.10R4.1.4} [Thermal Expansion] (ThermalExpansion-[1.7.10]4.1.4-247.jar) 
	UCHIJ	BuildCraft|Compat{7.1.5} [BuildCraft Compat] (buildcraft-compat-7.1.5.jar) 
	UCHIJ	cfm{3.4.8} [�9MrCrayfish's Furniture Mod] (cfm-3.4.8-mc1.7.10.jar) 
	UCHIJ	TwilightForest{2.3.7} [The Twilight Forest] (twilightforest-1.7.10-2.3.7.jar) 
	UCHIJ	ForgeMultipart{1.2.0.345} [Forge Multipart] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	UCHIJ	chisel{2.9.5.11} [Chisel] (Chisel-2.9.5.11.jar) 
	UCHIJ	CompactSolars{4.4.41.316} [Compact Solar Arrays] (CompactSolars-1.7.10-4.4.41.316-universal.jar) 
	UCHIJ	CustomSpawner{3.3.0} [DrZhark's CustomSpawner] (CustomMobSpawner 3.3.0.zip) 
	UCHIJ	PTRModelLib{1.0.0} [PTRModelLib] (Decocraft-2.3.6.1_1.7.10.jar) 
	UCHIJ	props{2.3.6.1} [Decocraft] (Decocraft-2.3.6.1_1.7.10.jar) 
	UCHIJ	MoCreatures{6.3.1} [DrZhark's Mo'Creatures Mod] (DrZharks MoCreatures Mod v6.3.1.zip) 
	UCHIJ	endercore{1.7.10-0.2.0.39_beta} [EnderCore] (EnderCore-1.7.10-0.2.0.39_beta.jar) 
	UCHIJ	Waila{1.5.10} [Waila] (Waila-1.5.10_1.7.10.jar) 
	UCHIJ	EnderIO{1.7.10-2.3.0.429_beta} [Ender IO] (EnderIO-1.7.10-2.3.0.429_beta.jar) 
	UCHIJ	EnderStorage{1.4.7.37} [EnderStorage] (EnderStorage-1.7.10-1.4.7.37-universal.jar) 
	UCHIJ	extracells{2.3.14} [Extra Cells 2] (ExtraCells-1.7.10-2.3.14b197.jar) 
	UCHIJ	ExtraUtilities{1.2.12} [Extra Utilities] (extrautilities-1.2.12.jar) 
	UCHIJ	IC2NuclearControl{2.4.2a} [Nuclear Control 2] (IC2NuclearControl-2.4.2a.jar) 
	UCHIJ	inventorytweaks{1.58-147-645ca10} [Inventory Tweaks] (InventoryTweaks-1.58-147.jar) 
	UCHIJ	IronChest{6.0.60.741} [Iron Chest] (Iron-Chests-Mod-1.7.10.jar) 
	UCHIJ	journeymap{5.1.4p1} [JourneyMap] (journeymap-1.7.10-5.1.4p1-unlimited.jar) 
	UCHIJ	koresample{1.7.10-1.3.2} [Kore Sample] (KoreSample-1.7.10-1.3.2.jar) 
	UCHIJ	Mekanism{9.1.0} [Mekanism] (Mekanism-1.7.10-9.1.0.281.jar) 
	UCHIJ	Morpheus{1.7.10-1.6.21} [Morpheus] (Morpheus-1.7.10-1.6.21.jar) 
	UCHIJ	NEIAddons{1.12.14.40} [NEI Addons] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJ	NEIAddons|Developer{1.12.14.40} [NEI Addons: Developer Tools] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJ	NEIAddons|AppEng{1.12.14.40} [NEI Addons: Applied Energistics 2] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJ	NEIAddons|Botany{1.12.14.40} [NEI Addons: Botany] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJ	NEIAddons|Forestry{1.12.14.40} [NEI Addons: Forestry] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJ	NEIAddons|CraftingTables{1.12.14.40} [NEI Addons: Crafting Tables] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJ	NEIAddons|ExNihilo{1.12.14.40} [NEI Addons: Ex Nihilo] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJ	neiintegration{1.1.2} [NEI Integration] (NEIIntegration-MC1.7.10-1.1.2.jar) 
	UCHIJ	NuclearCraft{1.9e} [NuclearCraft] (NuclearCraft-1.9e--1.7.10.jar) 
	UCHIJ	OpenMods{0.9.1} [OpenMods] (OpenModsLib-1.7.10-0.9.1.jar) 
	UCHIJ	OpenBlocks{1.5.1} [OpenBlocks] (OpenBlocks-1.7.10-1.5.1.jar) 
	UCHIJ	harvestcraft{1.7.10j} [Pam's HarvestCraft] (Pam's HarvestCraft 1.7.10Lb.jar) 
	UCHIJ	MrTJPCoreMod{1.1.0.33} [MrTJPCore] (MrTJPCore-1.1.0.33-universal.jar) 
	UCHIJ	ProjRed|Core{4.7.0pre12.95} [ProjectRed Core] (ProjectRed-1.7.10-4.7.0pre12.95-Base.jar) 
	UCHIJ	ProjRed|Transmission{4.7.0pre12.95} [ProjectRed Transmission] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJ	TConstruct{1.7.10-1.8.8.build988} [Tinkers' Construct] (TConstruct-1.7.10-1.8.8.jar) 
	UCHIJ	ProjRed|Compatibility{4.7.0pre12.95} [ProjectRed Compatibility] (ProjectRed-1.7.10-4.7.0pre12.95-Compat.jar) 
	UCHIJ	ProjRed|Integration{4.7.0pre12.95} [ProjectRed Integration] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJ	ProjRed|Fabrication{4.7.0pre12.95} [ProjectRed Fabrication] (ProjectRed-1.7.10-4.7.0pre12.95-Fabrication.jar) 
	UCHIJ	Roguelike{1.5.0} [Roguelike Dungeons] (roguelike-1.7.10-1.5.0b.jar) 
	UCHIJ	AS_Ruins{15.4} [Ruins Spawning System] (Ruins-1.7.10.jar) 
	UCHIJ	scottstweaks{1.7.10-1.3.1} [Scott's Tweaks] (ScottsTweaks-1.7.10-1.3.1.jar) 
	UCHIJ	StevesCarts{2.0.0.b18} [Steve's Carts 2] (StevesCarts2.0.0.b18.jar) 
	UCHIJ	StorageDrawers{1.7.10-1.10.8} [Storage Drawers] (StorageDrawers-1.7.10-1.10.8.jar) 
	UCHIJ	StorageDrawersBop{1.7.10-1.1.1} [Storage Drawers: Biomes O' Plenty Pack] (StorageDrawers-BiomesOPlenty-1.7.10-1.1.1.jar) 
	UCHIJ	StorageDrawersForestry{1.7.10-1.1.2} [Storage Drawers: Forestry Pack] (StorageDrawers-Forestry-1.7.10-1.1.2.jar) 
	UCHIJ	StorageDrawersMisc{1.7.10-1.1.2} [Storage Drawers: Misc Pack] (StorageDrawers-Misc-1.7.10-1.1.2.jar) 
	UCHIJ	StorageDrawersNatura{1.7.10-1.1.1} [Storage Drawers: Natura Pack] (StorageDrawers-Natura-1.7.10-1.1.1.jar) 
	UCHIJ	tcnodetracker{1.1.2} [TC Node Tracker] (tcnodetracker-1.7.10-1.1.2.jar) 
	UCHIJ	thaumcraftneiplugin{@VERSION@} [Thaumcraft NEI Plugin] (thaumcraftneiplugin-1.7.10-1.7a.jar) 
	UCHIE	thaumicenergistics{1.0.0.5-RV2} [Thaumic Energistics] (thaumicenergistics-1.0.0.5-RV2.jar) 
	UCHIJ	ThaumicTinkerer{unspecified} [Thaumic Tinkerer] (ThaumicTinkerer-2.5-1.7.10-164.jar) 
	UCHIJ	ThermalDynamics{1.7.10R1.2.0} [Thermal Dynamics] (ThermalDynamics-[1.7.10]1.2.0-171.jar) 
	UCHIJ	TiCTooltips{1.2.5} [TiC Tooltips] (TiCTooltips-mc1.7.10-1.2.5.jar) 
	UCHIJ	TMechworks{0.2.15.106} [Tinkers' Mechworks] (TMechworks-1.7.10-0.2.15.106.jar) 
	UCHIJ	WailaHarvestability{1.1.6} [Waila Harvestability] (WailaHarvestability-mc1.7.10-1.1.6.jar) 
	UCHIJ	wailaplugins{MC1.7.10-0.2.0-25} [WAILA Plugins] (WAILAPlugins-MC1.7.10-0.2.0-25.jar) 
	UCHIJ	wawla{1.3.1} [What Are We Looking At] (Wawla-1.0.5.120.jar) 
	UCHIJ	McMultipart{1.2.0.345} [Minecraft Multipart Plugin] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	UCHIJ	aobd{2.9.2} [Another One Bites The Dust] (AOBD-2.9.2.jar) 
	UCHIJ	ForgeMicroblock{1.2.0.345} [Forge Microblocks] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	GL info: ' Vendor: 'ATI Technologies Inc.' Version: '4.5.13416 Compatibility Profile Context 15.302' Renderer: 'AMD Radeon HD 7900 Series '
	OpenModsLib class transformers: [stencil_patches:FINISHED],[movement_callback:FINISHED],[map_gen_fix:FINISHED],[gl_capabilities_hook:FINISHED],[player_render_hook:FINISHED]
	Class transformer null safety: all safe
	AE2 Version: beta rv3-beta-6 for Forge 10.13.4.1448
	Mantle Environment: DO NOT REPORT THIS CRASH! Unsupported mods in environment: optifine
	CoFHCore: -[1.7.10]3.1.3-327
	ThermalFoundation: -[1.7.10]1.2.5-115
	ThermalExpansion: -[1.7.10]4.1.4-247
	TConstruct Environment: Environment healthy.
	ThermalDynamics: -[1.7.10]1.2.0-171
	List of loaded APIs: 
		* appliedenergistics2|API (rv3) from appliedenergistics2-rv3-beta-6.jar
		* AromaBackupAPI (1.0) from AromaBackup-1.7.10-0.1.0.0.jar
		* Baubles|API (1.0.1.10) from ThermalFoundation-[1.7.10]1.2.5-115.jar
		* BiomesOPlentyAPI (1.0.0) from BiomesOPlenty-1.7.10-2.1.0.2027-universal.jar
		* BotaniaAPI (76) from Botania r1.8-249.jar
		* BuildCraftAPI|blocks (1.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|blueprints (1.5) from buildcraft-7.1.19.jar
		* BuildCraftAPI|boards (2.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|core (1.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|crops (1.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|events (2.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|facades (1.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|filler (4.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|fuels (2.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|gates (4.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|items (1.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|library (2.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|lists (1.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|power (1.3) from buildcraft-7.1.19.jar
		* BuildCraftAPI|recipes (3.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|robotics (3.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|statements (1.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|tablet (1.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|tiles (1.2) from buildcraft-7.1.19.jar
		* BuildCraftAPI|tools (1.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|transport (4.1) from buildcraft-7.1.19.jar
		* ChiselAPI (0.1.1) from Chisel-2.9.5.11.jar
		* ChiselAPI|Carving (0.1.1) from Chisel-2.9.5.11.jar
		* ChiselAPI|Rendering (0.1.1) from Chisel-2.9.5.11.jar
		* CoFHAPI (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHAPI|block (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHAPI|core (1.7.10R1.3.1) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHAPI|energy (1.7.10R1.3.1) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHAPI|fluid (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHAPI|inventory (1.7.10R1.0.1) from buildcraft-compat-7.1.5.jar
		* CoFHAPI|item (1.7.10R1.3.1) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHAPI|modhelpers (1.7.10R1.3.1) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHAPI|tileentity (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHAPI|transport (1.7.10R1.0.13) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* CoFHAPI|world (1.7.10R1.3.1) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|audio (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|container (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|element (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|element|listbox (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|slot (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|inventory (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|render (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|render|particle (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|util (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|util|helpers (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|util|position (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|world (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|world|feature (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CSLib|API (0.3.0) from Decocraft-2.3.6.1_1.7.10.jar
		* EnderIOAPI (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* EnderIOAPI|Redstone (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* EnderIOAPI|Teleport (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* EnderIOAPI|Tools (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* ForestryAPI|apiculture (4.8.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|arboriculture (4.2.1) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|circuits (3.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|core (5.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|farming (2.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|food (1.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|fuels (2.0.1) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|genetics (4.7.1) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|hives (4.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|lepidopterology (1.3.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|mail (3.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|multiblock (3.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|recipes (5.4.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|storage (3.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|world (2.1.0) from forestry_1.7.10-4.2.16.64.jar
		* IC2API (1.0) from industrialcraft-2-2.2.827-experimental.jar
		* MekanismAPI|core (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|energy (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* MekanismAPI|gas (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* MekanismAPI|infuse (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|laser (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|reactor (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|recipe (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|transmitter (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* MekanismAPI|util (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* NuclearControlAPI (v1.0.5) from IC2NuclearControl-2.4.2a.jar
		* OpenBlocks|API (1.1) from OpenBlocks-1.7.10-1.5.1.jar
		* StorageDrawersAPI (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|config (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|event (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|inventory (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|pack (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|registry (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|render (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|storage (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|storage-attribute (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* Thaumcraft|API (4.2.2.0) from Thaumcraft-1.7.10-4.2.3.5.jar
		* thaumicenergistics|API (1.1) from thaumicenergistics-1.0.0.5-RV2.jar
		* WailaAPI (1.2) from Waila-1.5.10_1.7.10.jar
	Chisel: Errors like "[FML]: Unable to lookup ..." are NOT the cause of this crash. You can safely ignore these errors. And update forge while you're at it.
	EnderIO: Found the following problem(s) with your installation:
                  * Optifine is installed. This is NOT supported.
                  * An unknown AE2 API is installed (rv3 from appliedenergistics2-rv3-beta-6.jar).
                    Ender IO was build against API version rv2 and may or may not work with a newer version.
                 This may have caused the error. Try reproducing the crash WITHOUT this/these mod(s) before reporting it.
	Stencil buffer state: Function set: GL30, pool: forge, bits: 8
	Forestry : Warning: You have mods that change the behavior of Minecraft, ForgeModLoader, and/or Minecraft Forge to your client: 
Optifine
These may have caused this error, and may not be supported. Try reproducing the crash WITHOUT these mods, and report it then.
	AE2 Integration: IC2:ON, RotaryCraft:OFF, RC:OFF, BuildCraftCore:ON, BuildCraftTransport:ON, BuildCraftBuilder:ON, RF:ON, RFItem:ON, MFR:OFF, DSU:ON, FZ:OFF, FMP:ON, RB:OFF, CLApi:OFF, Waila:ON, InvTweaks:ON, NEI:ON, CraftGuide:OFF, Mekanism:ON, ImmibisMicroblocks:OFF, BetterStorage:OFF, OpenComputers:OFF, PneumaticCraft:OFF
	OptiFine Version: OptiFine_1.7.10_HD_U_D6
	Render Distance Chunks: 12
	Mipmaps: 4
	Anisotropic Filtering: 1
	Antialiasing: 0
	Multitexture: false
	OpenGlVersion: 4.5.13416 Compatibility Profile Context 15.302
	OpenGlRenderer: AMD Radeon HD 7900 Series 
	OpenGlVendor: ATI Technologies Inc.
	CpuCount: 8