---- Minecraft Crash Report ----
// Daisy, daisy...

Time: 2/3/17 3:33 PM
Description: Updating screen events

java.lang.NullPointerException: Updating screen events
	at net.minecraft.client.gui.GuiChat.func_73864_a(GuiChat.java:197)
	at net.minecraft.client.gui.GuiScreen.func_146274_d(GuiScreen.java:296)
	at net.minecraft.client.gui.GuiChat.func_146274_d(GuiChat.java:147)
	at net.minecraft.client.gui.GuiScreen.func_146269_k(GuiScreen.java:268)
	at net.minecraft.client.Minecraft.func_71407_l(Minecraft.java:1640)
	at net.minecraft.client.Minecraft.func_71411_J(Minecraft.java:973)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:898)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:236)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:297)
	at org.multimc.EntryPoint.listen(EntryPoint.java:162)
	at org.multimc.EntryPoint.main(EntryPoint.java:53)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at net.minecraft.client.gui.GuiChat.func_73864_a(GuiChat.java:197)
	at net.minecraft.client.gui.GuiScreen.func_146274_d(GuiScreen.java:296)
	at net.minecraft.client.gui.GuiChat.func_146274_d(GuiChat.java:147)
	at net.minecraft.client.gui.GuiScreen.func_146269_k(GuiScreen.java:268)
	at net.minecraft.client.Minecraft.func_71407_l(Minecraft.java:1640)

-- Affected screen --
Details:
	Screen name: net.minecraft.client.gui.GuiChat

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['Lightsockie'/6542, l='MpServer', x=-924.06, y=65.62, z=1234.29]]
	Chunk stats: MultiplayerChunkCache: 441, 441
	Level seed: 0
	Level generator: ID 04 - BIOMESOP, ver 0. Features enabled: false
	Level generator options: 
	Level spawn location: World: (-928,64,1231), Chunk: (at 0,4,15 in -58,76; contains blocks -928,0,1216 to -913,255,1231), Region: (-2,2; contains chunks -64,64 to -33,95, blocks -1024,0,1024 to -513,255,1535)
	Level time: 9747533 game time, 10713047 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: survival (ID 0). Hardcore: false. Cheats: false
	Forced entities: 42 total; [MoCEntityButterfly['ButterFly'/5764, l='MpServer', x=-927.47, y=67.00, z=1249.77], MoCEntityBigCat['BigCat'/5765, l='MpServer', x=-930.75, y=65.00, z=1294.53], EntitySkeleton['Skeleton'/8837, l='MpServer', x=-926.53, y=54.00, z=1263.13], EntitySkeleton['Skeleton'/8838, l='MpServer', x=-925.50, y=53.00, z=1264.50], MoCEntityKitty['Kitty'/5767, l='MpServer', x=-947.06, y=64.00, z=1291.38], EntityZombie['Zombie'/8213, l='MpServer', x=-868.50, y=27.00, z=1278.50], MoCEntityBoar['Boar'/5660, l='MpServer', x=-974.81, y=64.00, z=1213.25], MoCEntityHorse['WildHorse'/5661, l='MpServer', x=-986.63, y=69.00, z=1225.38], MoCEntitySnake['Snake'/5662, l='MpServer', x=-976.72, y=68.00, z=1249.38], MoCEntityBoar['Boar'/5663, l='MpServer', x=-984.19, y=68.00, z=1253.72], MoCEntityBigCat['BigCat'/5664, l='MpServer', x=-976.09, y=67.00, z=1253.53], MoCEntityBigCat['BigCat'/5665, l='MpServer', x=-973.59, y=65.00, z=1273.84], MoCEntityHorse['WildHorse'/5666, l='MpServer', x=-977.00, y=65.00, z=1290.38], MoCEntityBear['Bear'/5667, l='MpServer', x=-976.78, y=65.00, z=1292.66], MoCEntityButterfly['ButterFly'/5688, l='MpServer', x=-965.38, y=63.00, z=1225.09], MoCEntityBear['Bear'/5689, l='MpServer', x=-965.13, y=63.00, z=1244.78], MoCEntityBigCat['BigCat'/5690, l='MpServer', x=-967.22, y=65.00, z=1244.47], MoCEntitySnake['Snake'/5691, l='MpServer', x=-969.16, y=64.00, z=1246.34], MoCEntityBigCat['BigCat'/5692, l='MpServer', x=-973.91, y=66.00, z=1255.59], MoCEntityBear['Bear'/5693, l='MpServer', x=-972.09, y=64.00, z=1272.66], MoCEntityKitty['Kitty'/5694, l='MpServer', x=-964.44, y=64.00, z=1257.53], MoCEntityBoar['Boar'/5695, l='MpServer', x=-964.81, y=64.00, z=1281.22], MoCEntityFly['Fly'/5696, l='MpServer', x=-976.47, y=65.00, z=1269.88], MoCEntityBear['Bear'/5697, l='MpServer', x=-971.34, y=64.00, z=1286.59], MoCEntityBoar['Boar'/5698, l='MpServer', x=-970.19, y=64.00, z=1298.28], EntityCreeper['Creeper'/9420, l='MpServer', x=-935.84, y=16.00, z=1171.60], EntityCreeper['Creeper'/9421, l='MpServer', x=-937.50, y=16.00, z=1172.50], EntityCreeper['Creeper'/8913, l='MpServer', x=-910.50, y=60.00, z=1293.50], EntityClientPlayerMP['Lightsockie'/6542, l='MpServer', x=-924.06, y=65.62, z=1234.29], MoCEntitySnake['Snake'/5723, l='MpServer', x=-959.50, y=66.00, z=1226.50], MoCEntityHorse['WildHorse'/5724, l='MpServer', x=-950.72, y=67.00, z=1259.63], MoCEntityKitty['Kitty'/5725, l='MpServer', x=-956.00, y=64.00, z=1251.34], MoCEntityHorse['WildHorse'/5726, l='MpServer', x=-950.44, y=67.00, z=1274.22], MoCEntityBear['Bear'/5727, l='MpServer', x=-959.63, y=68.00, z=1265.13], MoCEntityBoar['Boar'/5728, l='MpServer', x=-959.31, y=67.00, z=1283.78], MoCEntitySnake['Snake'/5860, l='MpServer', x=-863.09, y=67.00, z=1282.34], EntityCreeper['Creeper'/6760, l='MpServer', x=-929.06, y=54.00, z=1266.22], EntityCreeper['Creeper'/6761, l='MpServer', x=-916.97, y=53.00, z=1261.47], EntityCreeper['Creeper'/6762, l='MpServer', x=-918.31, y=53.00, z=1262.38], EntityCreeper['Creeper'/6763, l='MpServer', x=-914.44, y=53.00, z=1269.03], EntityItem['item.item.string'/5867, l='MpServer', x=-863.81, y=11.13, z=1269.56], MoCEntityPiranha['Piranha'/5878, l='MpServer', x=-865.37, y=61.91, z=1226.18]]
	Retry entities: 0 total; []
	Server brand: HexaCord (git:BungeeCord-Bootstrap:1.11-SNAPSHOT:33cb9e3:125) <- thermos,cauldron,craftbukkit,mcpc,kcauldron,fml,forge
	Server type: Non-integrated multiplayer server
Stacktrace:
	at net.minecraft.client.Minecraft.func_71396_d(Minecraft.java:2444)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:919)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:236)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:297)
	at org.multimc.EntryPoint.listen(EntryPoint.java:162)
	at org.multimc.EntryPoint.main(EntryPoint.java:53)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Linux (amd64) version 4.8.13-1-ARCH
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 1284938600 bytes (1225 MB) / 3223322624 bytes (3074 MB) up to 3817865216 bytes (3641 MB)
	JVM Flags: 2 total; -Xms1024m -Xmx4096m
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 15, tallocated: 96
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1614 Optifine OptiFine_1.7.10_HD_U_D6 120 mods loaded, 120 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJA	FML{7.10.99.99} [Forge Mod Loader] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJA	Forge{10.13.4.1614} [Minecraft Forge] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJA	appliedenergistics2-core{rv3-beta-6} [Applied Energistics 2 Core] (minecraft.jar) 
	UCHIJA	Aroma1997Core{1.0.2.16} [Aroma1997Core] (Aroma1997Core-1.7.10-1.0.2.16.jar) 
	UCHIJA	CodeChickenCore{1.0.7.47} [CodeChicken Core] (minecraft.jar) 
	UCHIJA	InfiniBows{1.3.0 build 20} [InfiniBows] (minecraft.jar) 
	UCHIJA	NotEnoughItems{1.0.5.120} [Not Enough Items] (NotEnoughItems-1.7.10-1.0.5.120-universal.jar) 
	UCHIJA	ThE-core{1.0.0.1} [Thaumic Energistics Core] (minecraft.jar) 
	UCHIJA	ThaumicTinkerer-preloader{0.1} [Thaumic Tinkerer Core] (minecraft.jar) 
	UCHIJA	OpenModsCore{0.9.1} [OpenModsCore] (minecraft.jar) 
	UCHIJA	<CoFH ASM>{000} [CoFH ASM] (minecraft.jar) 
	UCHIJA	BinniePatcher{1.8.2} [Binnie Patcher] (minecraft.jar) 
	UCHIJA	FastCraft{1.23} [FastCraft] (fastcraft-1.23.jar) 
	UCHIJA	bspkrsCore{6.15} [bspkrsCore] ([1.7.10]bspkrsCore-universal-6.15.jar) 
	UCHIJA	ArmorStatusHUD{1.28} [ArmorStatusHUD] ([1.7.10]ArmorStatusHUD-client-1.28.jar) 
	UCHIJA	movillages{1.4.2} [Mo' Villages] ([1.7.10]MoVillages-1.4.2.jar) 
	UCHIJA	StatusEffectHUD{1.27} [StatusEffectHUD] ([1.7.10]StatusEffectHUD-client-1.27.jar) 
	UCHIJA	IC2{2.2.827-experimental} [IndustrialCraft 2] (industrialcraft-2-2.2.827-experimental.jar) 
	UCHIJA	AdvancedMachines{1.1.6} [IC2 Advanced Machines Addon] (AdvancedMachinesAS-1.7.10.jar) 
	UCHIJA	appliedenergistics2{rv3-beta-6} [Applied Energistics 2] (appliedenergistics2-rv3-beta-6.jar) 
	UCHIJA	bdlib{1.9.2.104} [BD Lib] (bdlib-1.9.2.104-mc1.7.10.jar) 
	UCHIJA	ae2stuff{0.5.0.56} [AE2 Stuff] (ae2stuff-0.5.0.56-mc1.7.10.jar) 
	UCHIJA	Aroma1997CoreHelper{1.0.2.16} [Aroma1997Core|Helper] (Aroma1997Core-1.7.10-1.0.2.16.jar) 
	UCHIJA	Aroma1997sDimension{1.0} [Aroma1997's Dimensional World] (Aroma1997s-Dimensional-World-1.7.10-1.1.0.1.jar) 
	UCHIJA	AromaBackup{0.1.0.0} [AromaBackup] (AromaBackup-1.7.10-0.1.0.0.jar) 
	UCHIJA	AromaBackupRecovery{1.0} [AromaBackup Recovery] (AromaBackup-1.7.10-0.1.0.0.jar) 
	UCHIJA	BiblioCraft{1.11.5} [BiblioCraft] (BiblioCraft[v1.11.5][MC1.7.10].jar) 
	UCHIJA	Mantle{1.7.10-0.3.2.jenkins191} [Mantle] (Mantle-1.7.10-0.3.2b.jar) 
	UCHIJA	Natura{2.2.0} [Natura] (natura-1.7.10-2.2.0.1.jar) 
	UCHIJA	BiomesOPlenty{2.1.0} [Biomes O' Plenty] (BiomesOPlenty-1.7.10-2.1.0.2027-universal.jar) 
	UCHIJA	BiblioWoodsBoP{1.9} [BiblioWoods Biomes O'Plenty Edition] (BiblioWoods[BiomesOPlenty][v1.9].jar) 
	UCHIJA	CoFHCore{1.7.10R3.1.3} [CoFH Core] (CoFHCore-[1.7.10]3.1.3-327.jar) 
	UCHIJA	Forestry{4.2.16.64} [Forestry for Minecraft] (forestry_1.7.10-4.2.16.64.jar) 
	UCHIJA	BiblioWoodsForestry{1.7} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.7].jar) 
	UCHIJA	BiblioWoodsNatura{1.5} [BiblioWoods Natura Edition] (BiblioWoods[Natura][v1.5].jar) 
	UCHIJA	BinnieCore{2.0-pre14} [Binnie Core] (binnie-mods-2.0-pre14.jar) 
	UCHIJA	Botany{2.0-pre14} [Botany] (binnie-mods-2.0-pre14.jar) 
	UCHIJA	ExtraBees{2.0-pre14} [Extra Bees] (binnie-mods-2.0-pre14.jar) 
	UCHIJA	ExtraTrees{2.0-pre14} [Extra Trees] (binnie-mods-2.0-pre14.jar) 
	UCHIJA	Genetics{2.0-pre14} [Genetics] (binnie-mods-2.0-pre14.jar) 
	UCHIJA	Baubles{1.0.1.10} [Baubles] (Baubles-1.7.10-1.0.1.10.jar) 
	UCHIJA	Thaumcraft{4.2.3.5} [Thaumcraft] (Thaumcraft-1.7.10-4.2.3.5.jar) 
	UCHIJA	Botania{r1.8-249} [Botania] (Botania r1.8-249.jar) 
	UCHIJA	BuildCraft|Core{7.1.19} [BuildCraft] (buildcraft-7.1.19.jar) 
	UCHIJA	BuildCraft|Energy{7.1.19} [BC Energy] (buildcraft-7.1.19.jar) 
	UCHIJA	BuildCraft|Silicon{7.1.19} [BC Silicon] (buildcraft-7.1.19.jar) 
	UCHIJA	BuildCraft|Builders{7.1.19} [BC Builders] (buildcraft-7.1.19.jar) 
	UCHIJA	BuildCraft|Robotics{7.1.19} [BC Robotics] (buildcraft-7.1.19.jar) 
	UCHIJA	BuildCraft|Transport{7.1.19} [BC Transport] (buildcraft-7.1.19.jar) 
	UCHIJA	BuildCraft|Factory{7.1.19} [BC Factory] (buildcraft-7.1.19.jar) 
	UCHIJA	ThermalFoundation{1.7.10R1.2.5} [Thermal Foundation] (ThermalFoundation-[1.7.10]1.2.5-115.jar) 
	UCHIJA	ThermalExpansion{1.7.10R4.1.4} [Thermal Expansion] (ThermalExpansion-[1.7.10]4.1.4-247.jar) 
	UCHIJA	BuildCraft|Compat{7.1.5} [BuildCraft Compat] (buildcraft-compat-7.1.5.jar) 
	UCHIJA	cfm{3.4.8} [�9MrCrayfish's Furniture Mod] (cfm-3.4.8-mc1.7.10.jar) 
	UCHIJA	Railcraft{9.12.2.0} [Railcraft] (Railcraft_1.7.10-9.12.2.0.jar) 
	UCHIJA	TwilightForest{2.3.7} [The Twilight Forest] (twilightforest-1.7.10-2.3.7.jar) 
	UCHIJA	ForgeMultipart{1.2.0.345} [Forge Multipart] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	UCHIJA	chisel{2.9.5.11} [Chisel] (Chisel-2.9.5.11.jar) 
	UCHIJA	CompactSolars{4.4.41.316} [Compact Solar Arrays] (CompactSolars-1.7.10-4.4.41.316-universal.jar) 
	UCHIJA	CustomSpawner{3.3.0} [DrZhark's CustomSpawner] (CustomMobSpawner 3.3.0.zip) 
	UCHIJA	PTRModelLib{1.0.0} [PTRModelLib] (Decocraft-2.3.6.1_1.7.10.jar) 
	UCHIJA	props{2.3.6.1} [Decocraft] (Decocraft-2.3.6.1_1.7.10.jar) 
	UCHIJA	MoCreatures{6.3.1} [DrZhark's Mo'Creatures Mod] (DrZharks MoCreatures Mod v6.3.1.zip) 
	UCHIJA	endercore{1.7.10-0.2.0.39_beta} [EnderCore] (EnderCore-1.7.10-0.2.0.39_beta.jar) 
	UCHIJA	Waila{1.5.10} [Waila] (Waila-1.5.10_1.7.10.jar) 
	UCHIJA	EnderIO{1.7.10-2.3.0.429_beta} [Ender IO] (EnderIO-1.7.10-2.3.0.429_beta.jar) 
	UCHIJA	EnderStorage{1.4.7.37} [EnderStorage] (EnderStorage-1.7.10-1.4.7.37-universal.jar) 
	UCHIJA	extracells{2.3.14} [Extra Cells 2] (ExtraCells-1.7.10-2.3.14b197.jar) 
	UCHIJA	ExtraUtilities{1.2.12} [Extra Utilities] (extrautilities-1.2.12.jar) 
	UCHIJA	fastleafdecay{1.4} [Fast Leaf Decay] (FastLeafDecay-1.7.10-1.4.jar) 
	UCHIJA	IC2NuclearControl{2.4.2a} [Nuclear Control 2] (IC2NuclearControl-2.4.2a.jar) 
	UCHIJA	IGWMod{1.1.12-34} [In-Game wiki Mod] (IGW-Mod-1.7.10-1.1.12-34-universal.jar) 
	UCHIJA	inventorytweaks{1.58-147-645ca10} [Inventory Tweaks] (InventoryTweaks-1.58-147.jar) 
	UCHIJA	IronChest{6.0.60.741} [Iron Chest] (Iron-Chests-Mod-1.7.10.jar) 
	UCHIJA	journeymap{5.1.4p1} [JourneyMap] (journeymap-1.7.10-5.1.4p1-unlimited.jar) 
	UCHIJA	koresample{1.7.10-1.3.2} [Kore Sample] (KoreSample-1.7.10-1.3.2.jar) 
	UCHIJA	Mekanism{9.1.0} [Mekanism] (Mekanism-1.7.10-9.1.0.281.jar) 
	UCHIJA	Morpheus{1.7.10-1.6.21} [Morpheus] (Morpheus-1.7.10-1.6.21.jar) 
	UCHIJA	NEIAddons{1.12.14.40} [NEI Addons] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Developer{1.12.14.40} [NEI Addons: Developer Tools] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|AppEng{1.12.14.40} [NEI Addons: Applied Energistics 2] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Botany{1.12.14.40} [NEI Addons: Botany] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Forestry{1.12.14.40} [NEI Addons: Forestry] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|CraftingTables{1.12.14.40} [NEI Addons: Crafting Tables] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|ExNihilo{1.12.14.40} [NEI Addons: Ex Nihilo] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	neiintegration{1.1.2} [NEI Integration] (NEIIntegration-MC1.7.10-1.1.2.jar) 
	UCHIJA	NuclearCraft{1.9e} [NuclearCraft] (NuclearCraft-1.9e--1.7.10.jar) 
	UCHIJA	OpenMods{0.9.1} [OpenMods] (OpenModsLib-1.7.10-0.9.1.jar) 
	UCHIJA	OpenBlocks{1.5.1} [OpenBlocks] (OpenBlocks-1.7.10-1.5.1.jar) 
	UCHIJA	harvestcraft{1.7.10j} [Pam's HarvestCraft] (Pam's HarvestCraft 1.7.10Lb.jar) 
	UCHIJA	MrTJPCoreMod{1.1.0.33} [MrTJPCore] (MrTJPCore-1.1.0.33-universal.jar) 
	UCHIJA	ProjRed|Core{4.7.0pre12.95} [ProjectRed Core] (ProjectRed-1.7.10-4.7.0pre12.95-Base.jar) 
	UCHIJA	ProjRed|Transmission{4.7.0pre12.95} [ProjectRed Transmission] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJA	TConstruct{1.7.10-1.8.8.build988} [Tinkers' Construct] (TConstruct-1.7.10-1.8.8.jar) 
	UCHIJA	ProjRed|Compatibility{4.7.0pre12.95} [ProjectRed Compatibility] (ProjectRed-1.7.10-4.7.0pre12.95-Compat.jar) 
	UCHIJA	ProjRed|Integration{4.7.0pre12.95} [ProjectRed Integration] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJA	ProjRed|Fabrication{4.7.0pre12.95} [ProjectRed Fabrication] (ProjectRed-1.7.10-4.7.0pre12.95-Fabrication.jar) 
	UCHIJA	Roguelike{1.5.0} [Roguelike Dungeons] (roguelike-1.7.10-1.5.0b.jar) 
	UCHIJA	AS_Ruins{15.4} [Ruins Spawning System] (Ruins-1.7.10.jar) 
	UCHIJA	scottstweaks{1.7.10-1.3.1} [Scott's Tweaks] (ScottsTweaks-1.7.10-1.3.1.jar) 
	UCHIJA	StevesCarts{2.0.0.b18} [Steve's Carts 2] (StevesCarts2.0.0.b18.jar) 
	UCHIJA	StorageDrawers{1.7.10-1.10.8} [Storage Drawers] (StorageDrawers-1.7.10-1.10.8.jar) 
	UCHIJA	StorageDrawersBop{1.7.10-1.1.1} [Storage Drawers: Biomes O' Plenty Pack] (StorageDrawers-BiomesOPlenty-1.7.10-1.1.1.jar) 
	UCHIJA	StorageDrawersForestry{1.7.10-1.1.2} [Storage Drawers: Forestry Pack] (StorageDrawers-Forestry-1.7.10-1.1.2.jar) 
	UCHIJA	StorageDrawersMisc{1.7.10-1.1.2} [Storage Drawers: Misc Pack] (StorageDrawers-Misc-1.7.10-1.1.2.jar) 
	UCHIJA	StorageDrawersNatura{1.7.10-1.1.1} [Storage Drawers: Natura Pack] (StorageDrawers-Natura-1.7.10-1.1.1.jar) 
	UCHIJA	tcnodetracker{1.1.2} [TC Node Tracker] (tcnodetracker-1.7.10-1.1.2.jar) 
	UCHIJA	thaumcraftneiplugin{@VERSION@} [Thaumcraft NEI Plugin] (thaumcraftneiplugin-1.7.10-1.7a.jar) 
	UCHIJA	thaumicenergistics{1.0.0.5} [Thaumic Energistics] (thaumicenergistics-1.0.0.5.jar) 
	UCHIJA	ThaumicTinkerer{unspecified} [Thaumic Tinkerer] (ThaumicTinkerer-2.5-1.7.10-164.jar) 
	UCHIJA	ThermalDynamics{1.7.10R1.2.0} [Thermal Dynamics] (ThermalDynamics-[1.7.10]1.2.0-171.jar) 
	UCHIJA	TiCTooltips{1.2.5} [TiC Tooltips] (TiCTooltips-mc1.7.10-1.2.5.jar) 
	UCHIJA	TMechworks{0.2.15.106} [Tinkers' Mechworks] (TMechworks-1.7.10-0.2.15.106.jar) 
	UCHIJA	WailaHarvestability{1.1.6} [Waila Harvestability] (WailaHarvestability-mc1.7.10-1.1.6.jar) 
	UCHIJA	wailaplugins{MC1.7.10-0.2.0-25} [WAILA Plugins] (WAILAPlugins-MC1.7.10-0.2.0-25.jar) 
	UCHIJA	wawla{1.3.1} [What Are We Looking At] (Wawla-1.0.5.120.jar) 
	UCHIJA	McMultipart{1.2.0.345} [Minecraft Multipart Plugin] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	UCHIJA	aobd{2.9.2} [Another One Bites The Dust] (AOBD-2.9.2.jar) 
	UCHIJA	ForgeMicroblock{1.2.0.345} [Forge Microblocks] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	GL info: ' Vendor: 'ATI Technologies Inc.' Version: '4.5.13416 Compatibility Profile Context 15.302' Renderer: 'AMD Radeon HD 7900 Series '
	OpenModsLib class transformers: [stencil_patches:FINISHED],[movement_callback:FINISHED],[map_gen_fix:FINISHED],[gl_capabilities_hook:FINISHED],[player_render_hook:FINISHED]
	Class transformer null safety: all safe
	AE2 Version: beta rv3-beta-6 for Forge 10.13.4.1448
	Mantle Environment: DO NOT REPORT THIS CRASH! Unsupported mods in environment: optifine
	CoFHCore: -[1.7.10]3.1.3-327
	ThermalFoundation: -[1.7.10]1.2.5-115
	ThermalExpansion: -[1.7.10]4.1.4-247
	TConstruct Environment: Environment healthy.
	ThermalDynamics: -[1.7.10]1.2.0-171
	List of loaded APIs: 
		* appliedenergistics2|API (rv2) from ThaumicTinkerer-2.5-1.7.10-164.jar
		* AromaBackupAPI (1.0) from AromaBackup-1.7.10-0.1.0.0.jar
		* Baubles|API (1.0.1.10) from ThermalFoundation-[1.7.10]1.2.5-115.jar
		* BiomesOPlentyAPI (1.0.0) from BiomesOPlenty-1.7.10-2.1.0.2027-universal.jar
		* BotaniaAPI (76) from Botania r1.8-249.jar
		* BuildCraftAPI|blocks (1.0) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|blueprints (1.5) from buildcraft-7.1.19.jar
		* BuildCraftAPI|boards (2.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|core (1.0) from extrautilities-1.2.12.jar
		* BuildCraftAPI|crops (1.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|events (2.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|facades (1.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|filler (4.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|fuels (2.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|gates (4.1) from buildcraft-7.1.19.jar
		* BuildCraftAPI|items (1.1) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|library (2.0) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|lists (1.0) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|power (1.3) from buildcraft-7.1.19.jar
		* BuildCraftAPI|recipes (3.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|robotics (3.0) from buildcraft-7.1.19.jar
		* BuildCraftAPI|statements (1.1) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|tablet (1.0) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|tiles (1.2) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|tools (1.0) from Railcraft_1.7.10-9.12.2.0.jar
		* BuildCraftAPI|transport (4.1) from buildcraft-7.1.19.jar
		* ChiselAPI (0.1.1) from Chisel-2.9.5.11.jar
		* ChiselAPI|Carving (0.1.1) from Chisel-2.9.5.11.jar
		* ChiselAPI|Rendering (0.1.1) from Chisel-2.9.5.11.jar
		* CoFHAPI (1.7.10R1.0.13) from EnderCore-1.7.10-0.2.0.39_beta.jar
		* CoFHAPI|block (1.7.10R1.0.13) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* CoFHAPI|core (1.7.10R1.3.1) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHAPI|energy (1.7.10R1.0.2) from buildcraft-7.1.19.jar
		* CoFHAPI|fluid (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHAPI|inventory (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHAPI|item (1.7.10R1.0.10) from Mekanism-1.7.10-9.1.0.281.jar
		* CoFHAPI|modhelpers (1.7.10R1.3.1) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHAPI|tileentity (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHAPI|transport (1.7.10R1.0.13) from EnderCore-1.7.10-0.2.0.39_beta.jar
		* CoFHAPI|world (1.7.10R1.1.0) from NuclearCraft-1.9e--1.7.10.jar
		* CoFHLib (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|audio (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|container (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|element (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|element|listbox (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|gui|slot (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|inventory (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|render (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|render|particle (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|util (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|util|helpers (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|util|position (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|world (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CoFHLib|world|feature (1.7.10R1.1.2) from CoFHCore-[1.7.10]3.1.3-327.jar
		* CSLib|API (0.3.0) from Decocraft-2.3.6.1_1.7.10.jar
		* EnderIOAPI (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* EnderIOAPI|Redstone (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* EnderIOAPI|Teleport (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* EnderIOAPI|Tools (0.0.2) from EnderIO-1.7.10-2.3.0.429_beta.jar
		* ForestryAPI|apiculture (4.8.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|arboriculture (4.2.1) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|circuits (3.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|core (5.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|farming (2.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|food (1.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|fuels (2.0.1) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|genetics (4.7.1) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|hives (4.1.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|lepidopterology (1.3.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|mail (3.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|multiblock (3.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|recipes (5.4.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|storage (3.0.0) from forestry_1.7.10-4.2.16.64.jar
		* ForestryAPI|world (2.1.0) from forestry_1.7.10-4.2.16.64.jar
		* IC2API (1.0) from industrialcraft-2-2.2.827-experimental.jar
		* MekanismAPI|core (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|energy (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* MekanismAPI|gas (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|infuse (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|laser (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* MekanismAPI|reactor (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* MekanismAPI|recipe (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* MekanismAPI|transmitter (9.0.0) from NuclearCraft-1.9e--1.7.10.jar
		* MekanismAPI|util (9.0.0) from Mekanism-1.7.10-9.1.0.281.jar
		* NuclearControlAPI (v1.0.5) from IC2NuclearControl-2.4.2a.jar
		* OpenBlocks|API (1.1) from OpenBlocks-1.7.10-1.5.1.jar
		* RailcraftAPI|bore (1.0.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|carts (1.6.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|core (1.5.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|crafting (1.0.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|electricity (2.0.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|events (1.0.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|fuel (1.0.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|helpers (1.1.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|items (1.0.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|locomotive (1.1.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|signals (2.3.0) from Railcraft_1.7.10-9.12.2.0.jar
		* RailcraftAPI|tracks (2.3.0) from Railcraft_1.7.10-9.12.2.0.jar
		* StorageDrawersAPI (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|config (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|event (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|inventory (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|pack (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|registry (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|render (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|storage (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* StorageDrawersAPI|storage-attribute (1.7.10-1.2.0) from StorageDrawers-1.7.10-1.10.8.jar
		* Thaumcraft|API (4.2.2.0) from Railcraft_1.7.10-9.12.2.0.jar
		* thaumicenergistics|API (1.1) from thaumicenergistics-1.0.0.5.jar
		* WailaAPI (1.2) from Waila-1.5.10_1.7.10.jar
	Chisel: Errors like "[FML]: Unable to lookup ..." are NOT the cause of this crash. You can safely ignore these errors. And update forge while you're at it.
	EnderIO: Found the following problem(s) with your installation:
                  * Optifine is installed. This is NOT supported.
                  * The RF API that is being used (1.7.10R1.3.1 from <unknown>) differes from that that is reported as being loaded (1.7.10R1.0.2 from buildcraft-7.1.19.jar).
                    It is a supported version, but that difference may lead to problems.
                 This may have caused the error. Try reproducing the crash WITHOUT this/these mod(s) before reporting it.
	Stencil buffer state: Function set: GL30, pool: forge, bits: 8
	Forestry : Warning: You have mods that change the behavior of Minecraft, ForgeModLoader, and/or Minecraft Forge to your client: 
Optifine
These may have caused this error, and may not be supported. Try reproducing the crash WITHOUT these mods, and report it then.
	AE2 Integration: IC2:ON, RotaryCraft:OFF, RC:ON, BuildCraftCore:ON, BuildCraftTransport:ON, BuildCraftBuilder:ON, RF:ON, RFItem:ON, MFR:OFF, DSU:ON, FZ:OFF, FMP:ON, RB:OFF, CLApi:OFF, Waila:ON, InvTweaks:ON, NEI:ON, CraftGuide:OFF, Mekanism:ON, ImmibisMicroblocks:OFF, BetterStorage:OFF, OpenComputers:OFF, PneumaticCraft:OFF
	Launched Version: MultiMC5
	LWJGL: 2.9.1
	OpenGL: AMD Radeon HD 7900 Series  GL version 4.5.13416 Compatibility Profile Context 15.302, ATI Technologies Inc.
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)
	OptiFine Version: OptiFine_1.7.10_HD_U_D6
	Render Distance Chunks: 12
	Mipmaps: 4
	Anisotropic Filtering: 1
	Antialiasing: 0
	Multitexture: false
	OpenGlVersion: 4.5.13416 Compatibility Profile Context 15.302
	OpenGlRenderer: AMD Radeon HD 7900 Series 
	OpenGlVendor: ATI Technologies Inc.
	CpuCount: 8